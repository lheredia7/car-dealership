package cardealership;

import io.agroal.api.AgroalDataSource;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Response;

import java.net.URI;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.NoSuchElementException;

@Path("/cardealership")
@Transactional
public class VehicleResource {
    private VehicleRepository vehicleRepository;
    private VehicleMapper mapper;
    private VehicleValidator validator;

    @Inject
    public VehicleResource(VehicleRepository vehicleRepository, VehicleMapper mapper, VehicleValidator validator) {
        this.vehicleRepository = vehicleRepository;
        this.mapper = mapper;
        this.validator = validator;
    }

    @Inject
    AgroalDataSource agroalDataSource;

    @GET
    public List<Vehicle> index() {
        return vehicleRepository.listAll();
    }

    @GET
    @Path("{plate}")
    public Response retrieve(@PathParam("plate") String plate){
        var vehicle = vehicleRepository.find("plate", plate).firstResult();
        if(vehicle != null){
            return Response.created(URI.create("/cardealership/" + vehicle.getId())).entity(vehicle).build();
        }
        return Response.status(404).entity("Plate '" + plate + "' not found").build();
    }

    @POST
    public Response create(CreateVehicleDto insertedVehicle) throws SQLException {
        var error = this.validator.validatePostVehicle(insertedVehicle);
        if(error.isPresent()){
            var msg = error.get();
            return Response.status(400).entity(msg).build();
        }

        var entity = mapper.fromCreate(insertedVehicle);

        if(vehicleRepository.find("plate", entity.getPlate()).firstResult() != null) {
            return Response.status(400).entity("Plate '" + entity.getPlate() + "' already exists").build();
        }
        Connection conn = agroalDataSource.getConnection();
        String sql ="INSERT INTO cardealership.vehicle (plate, type, brand, model, color, productionDate) " +
                "VALUES ('" + entity.getPlate() + "', '" + entity.getType() + "', '" + entity.getBrand() + "', '" + entity.getModel() + "', '" + entity.getColor() + "', '" + entity.getProductionDate() + "')";
        Statement statement = conn.createStatement();
        statement.execute(sql);
        var vehicle = vehicleRepository.find("plate", entity.getPlate()).firstResult();
        return Response.created(URI.create("/cardealership/" + vehicle.getId())).entity(vehicle).build();
    }

    /*
    @POST
    public Response create(CreateVehicleDto insertedVehicle) {
        var entity = mapper.fromCreate(insertedVehicle);
        vehicleRepository.persist(entity);
        return Response.created(URI.create("/cardealership/"+ entity.getId())).entity(entity).build();
    }
    */

    @PUT
    @Path("{plate}")
    public Response update(@PathParam("plate") String plate, UpdateVehicleDto vehicle) {
        var error = this.validator.validatePutVehicle(vehicle);
        if(error.isPresent()){
            var msg = error.get();
            return Response.status(400).entity(msg).build();
        }
        var updatedVehicle = vehicleRepository.find("plate", plate).firstResult();
        if(updatedVehicle != null) {
            mapper.update(vehicle, updatedVehicle);
            vehicleRepository.persist(updatedVehicle);
            Vehicle vehicleUpdated = vehicleRepository.find("plate", plate).firstResult();
            return Response.created(URI.create("/cardealership/" + vehicleUpdated.getId())).entity(vehicleUpdated).build();
        }
        return Response.status(404).entity("Plate '" + plate + "' not found").build();
    }

    @DELETE
    @Path("{plate}")
    public Response delete(@PathParam("plate") @NotNull(message = "Plate should not be blank") String plate) {
        Vehicle vehicleToDelete = vehicleRepository.find("plate", plate).firstResult();
        if(vehicleToDelete != null){
            if(vehicleRepository.deleteById(vehicleToDelete.getId())) {
                return Response.ok("Vehicle deleted correctly.").build();
            } else {
                return Response.status(400).entity("Could not delete the item").build();
            }
        }
        return Response.status(404).entity("Plate '" + plate + "' not found").build();
    }
}
