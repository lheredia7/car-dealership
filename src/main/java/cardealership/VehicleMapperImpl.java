package cardealership;

import jakarta.enterprise.context.RequestScoped;

@RequestScoped
public class VehicleMapperImpl implements VehicleMapper{
    @Override
    public Vehicle fromCreate(CreateVehicleDto dto) {
        Vehicle vehicle = new Vehicle();
        vehicle.setPlate(dto.plate());
        vehicle.setType(dto.type());
        vehicle.setBrand(dto.brand());
        vehicle.setModel(dto.model());
        vehicle.setColor(dto.color());
        vehicle.setProductionDate(dto.productionDate());
        return vehicle;
    }

    @Override
    public void update(UpdateVehicleDto dto, Vehicle vehicle) {
        vehicle.setType(dto.type());
        vehicle.setBrand(dto.brand());
        vehicle.setModel(dto.model());
        vehicle.setColor(dto.color());
        vehicle.setProductionDate(dto.productionDate());
    }
}
