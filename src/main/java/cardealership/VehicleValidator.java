package cardealership;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.validation.Validator;

import java.util.Optional;

@ApplicationScoped
public class VehicleValidator {
    Validator validator;

    @Inject
    public VehicleValidator(Validator validator){
        this.validator = validator;
    }

    public Optional<String> validatePostVehicle(CreateVehicleDto vehicle) {
        var errors = validator.validate(vehicle);
        if(errors.isEmpty()){
            return Optional.empty();
        }
        return Optional.of(errors.stream().findFirst().get().getMessage());
    }

    public Optional<String> validatePutVehicle(UpdateVehicleDto vehicle) {
        var errors = validator.validate(vehicle);
        if(errors.isEmpty()){
            return Optional.empty();
        }
        return Optional.of(errors.stream().findFirst().get().getMessage());
    }

}
