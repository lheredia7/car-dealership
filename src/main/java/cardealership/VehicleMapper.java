package cardealership;

public interface VehicleMapper {
    Vehicle fromCreate(CreateVehicleDto dto);

    void update(UpdateVehicleDto dto, Vehicle vehicle);
}
