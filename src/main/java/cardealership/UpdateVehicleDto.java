package cardealership;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PastOrPresent;

import java.time.LocalDate;

public record UpdateVehicleDto(
        @NotBlank(message = "Type should not be blank") String type
        , @NotBlank(message = "Brand should not be blank") String brand
        , @NotBlank(message = "Model should not be blank") String model
        , @NotBlank(message = "Color should not be blank") String color
        , @NotNull(message = "Production Date should not be blank") @PastOrPresent(message = "Production date should be past or present") LocalDate productionDate) {
}
